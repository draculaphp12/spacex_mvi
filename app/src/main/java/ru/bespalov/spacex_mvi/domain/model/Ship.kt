package ru.bespalov.spacex_mvi.domain.model

data class Ship(
    val id: String?,
    val name: String?,
    val model: String?,
    val image: String?,
)
