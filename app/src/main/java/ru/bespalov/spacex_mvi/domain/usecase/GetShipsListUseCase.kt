package ru.bespalov.spacex_mvi.domain.usecase

import ru.bespalov.spacex_mvi.domain.model.Ship
import ru.bespalov.spacex_mvi.domain.repository.NetworkRepository
import javax.inject.Inject

class GetShipsListUseCase @Inject constructor(
    private val networkRepository: NetworkRepository
) {
    suspend fun execute(searchName: String = ""): List<Ship> {
        return networkRepository.getShipsList(searchName)
    }
}