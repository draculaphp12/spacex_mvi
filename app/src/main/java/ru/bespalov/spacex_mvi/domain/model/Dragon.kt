package ru.bespalov.spacex_mvi.domain.model

data class Dragon(
    val id: String?,
    val name: String?,
    val description: String?,
)