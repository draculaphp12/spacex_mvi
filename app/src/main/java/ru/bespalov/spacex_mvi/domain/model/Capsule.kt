package ru.bespalov.spacex_mvi.domain.model


data class Capsule(
    val id: String?,
    val dragon: Dragon?,
    val status: String?,
    val type: String?,
    val missions: List<CapsuleMission>?
)
