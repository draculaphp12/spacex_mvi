package ru.bespalov.spacex_mvi.domain.model

data class CapsuleMission(
    val name: String?,
    val flight: Int?
)
