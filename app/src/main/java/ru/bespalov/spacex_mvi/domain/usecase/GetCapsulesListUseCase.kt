package ru.bespalov.spacex_mvi.domain.usecase

import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.domain.repository.NetworkRepository
import javax.inject.Inject

class GetCapsulesListUseCase @Inject constructor(
    private val networkRepository: NetworkRepository
) {
    suspend fun execute(searchId: String = ""): List<Capsule> {
        return networkRepository.getCapsulesList(searchId)
    }
}