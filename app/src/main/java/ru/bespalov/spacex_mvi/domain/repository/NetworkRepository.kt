package ru.bespalov.spacex_mvi.domain.repository

import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.domain.model.Ship

interface NetworkRepository {
    suspend fun getCapsulesList(searchId: String = ""): List<Capsule>

    suspend fun getShipsList(searchName: String = ""): List<Ship>
}