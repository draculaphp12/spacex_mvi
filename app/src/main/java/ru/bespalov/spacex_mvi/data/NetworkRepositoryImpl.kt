package ru.bespalov.spacex_mvi.data

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import ru.bespalov.spacex_mvi.CapsulesListQuery
import ru.bespalov.spacex_mvi.ShipsListQuery
import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.domain.model.Ship
import ru.bespalov.spacex_mvi.domain.repository.NetworkRepository
import ru.bespalov.spacex_mvi.type.CapsulesFind
import ru.bespalov.spacex_mvi.type.ShipsFind
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor(
    private val apolloClient: ApolloClient
) : NetworkRepository {
    override suspend fun getCapsulesList(searchId: String): List<Capsule> {
        val capsules = apolloClient.query(
            CapsulesListQuery(
                find = Optional.present(CapsulesFind(id = Optional.present(searchId)))
            )
        ).execute().data?.capsules
            ?.filterNotNull()
            ?.map {
                it.toCapsule()
            } ?: emptyList()
        return capsules
    }

    override suspend fun getShipsList(searchName: String): List<Ship> {
        val ships = apolloClient.query(
            ShipsListQuery(
                find = Optional.present(ShipsFind(name = Optional.present(searchName)))
            )
        ).execute().data?.ships
            ?.filterNotNull()
            ?.map {
                it.toShip()
            } ?: emptyList()
        return ships
    }
}