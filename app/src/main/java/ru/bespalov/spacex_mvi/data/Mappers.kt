package ru.bespalov.spacex_mvi.data

import ru.bespalov.spacex_mvi.CapsulesListQuery
import ru.bespalov.spacex_mvi.ShipsListQuery
import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.domain.model.Dragon
import ru.bespalov.spacex_mvi.domain.model.CapsuleMission
import ru.bespalov.spacex_mvi.domain.model.Ship

fun CapsulesListQuery.Capsule.toCapsule(): Capsule = Capsule(
    id,
    dragon?.toDragon(),
    status,
    type,
    missions
        ?.filterNotNull()
        ?.map {
            it.toMission()
        }
)

fun CapsulesListQuery.Dragon.toDragon(): Dragon = Dragon(
    id, name, description
)

fun CapsulesListQuery.Mission.toMission(): CapsuleMission = CapsuleMission(
    name, flight
)

fun ShipsListQuery.Ship.toShip(): Ship = Ship(
    id, name, model, image
)