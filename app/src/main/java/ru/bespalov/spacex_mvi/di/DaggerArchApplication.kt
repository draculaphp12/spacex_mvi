package ru.bespalov.spacex_mvi.di

import android.app.Application
import android.content.Context

class DaggerArchApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        AppComponent.init(
            DaggerAppComponent.builder()
                .build()
        )
    }

    companion object {
        @Volatile
        lateinit var appContext: Context
            private set
    }
}