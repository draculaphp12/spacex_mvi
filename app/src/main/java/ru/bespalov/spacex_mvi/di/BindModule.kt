package ru.bespalov.spacex_mvi.di

import dagger.Binds
import dagger.Module
import ru.bespalov.spacex_mvi.data.NetworkRepositoryImpl
import ru.bespalov.spacex_mvi.domain.repository.NetworkRepository

@Module
abstract class BindModule {
    @Binds
    abstract fun bindCapsuleRepository(networkRepositoryImpl: NetworkRepositoryImpl): NetworkRepository
}