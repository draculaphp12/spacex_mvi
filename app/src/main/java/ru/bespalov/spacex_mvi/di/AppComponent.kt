package ru.bespalov.spacex_mvi.di

import dagger.Component
import dagger.internal.Preconditions
import ru.bespalov.spacex_mvi.presentation.MainActivity
import ru.bespalov.spacex_mvi.presentation.capsules_list.CapsuleViewModelFactory
import ru.bespalov.spacex_mvi.presentation.capsules_list.CapsulesListFragment
import ru.bespalov.spacex_mvi.presentation.ships_list.ShipsListFragment
import ru.bespalov.spacex_mvi.presentation.ships_list.ShipsListViewModelFactory
import javax.inject.Singleton

@Component(
    modules = [
        AppModule::class,
        BindModule::class
    ]
)
@Singleton
abstract class AppComponent {
    abstract fun inject(daggerArchApplication: DaggerArchApplication)

    abstract fun inject(activity: MainActivity)

    abstract fun inject(fragment: CapsulesListFragment)

    abstract fun inject(fragment: ShipsListFragment)

    abstract fun capsuleViewModelFactory(): CapsuleViewModelFactory

    abstract fun shipsListViewModelFactory(): ShipsListViewModelFactory

    companion object {
        @Volatile
        private var instance: AppComponent? = null

        fun get(): AppComponent {
            return Preconditions.checkNotNull(instance,
                "AppComponent is not initialized yet. Call init first.")!!
        }

        fun init(component: AppComponent) {
            require(instance == null) { "AppComponent is already initialized." }
            instance = component
        }
    }
}