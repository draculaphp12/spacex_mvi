package ru.bespalov.spacex_mvi.di

import android.content.Context
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.cache.normalized.normalizedCache
import com.apollographql.apollo3.cache.normalized.sql.SqlNormalizedCacheFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun provideContext(): Context {
        return DaggerArchApplication.appContext
    }

    @Singleton
    @Provides
    fun provideApolloClient(): ApolloClient {
        val sqlNormalizedCacheFactory = SqlNormalizedCacheFactory("apollo.db")
        return ApolloClient.Builder()
            .serverUrl("https://api.spacex.land/graphql")
            .normalizedCache(sqlNormalizedCacheFactory)
            .build()
    }
}