package ru.bespalov.spacex_mvi.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.bespalov.spacex_mvi.R
import ru.bespalov.spacex_mvi.databinding.FragmentMainBinding
import ru.bespalov.spacex_mvi.presentation.capsules_list.CapsulesListFragment
import ru.bespalov.spacex_mvi.presentation.ships_list.ShipsListFragment

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.main_title)
        binding.capsulesButton.setOnClickListener {
            parentFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.mainContainer, CapsulesListFragment())
                .commit()
        }
        binding.shipsButton.setOnClickListener {
            parentFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.mainContainer, ShipsListFragment())
                .commit()
        }
    }
}