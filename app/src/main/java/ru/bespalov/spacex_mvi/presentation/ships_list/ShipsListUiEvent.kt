package ru.bespalov.spacex_mvi.presentation.ships_list

import ru.bespalov.spacex_mvi.presentation.base.UiEvent

sealed class ShipsListUiEvent : UiEvent {
    data class ChangeSearch(val searchString: String = ""): ShipsListUiEvent()
}