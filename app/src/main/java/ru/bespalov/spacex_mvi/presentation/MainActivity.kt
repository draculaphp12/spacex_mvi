package ru.bespalov.spacex_mvi.presentation

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch
import ru.bespalov.spacex_mvi.R
import ru.bespalov.spacex_mvi.databinding.ActivityMainBinding
import ru.bespalov.spacex_mvi.di.AppComponent
import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.presentation.capsules_list.CapsuleViewModel
import ru.bespalov.spacex_mvi.presentation.capsules_list.CapsulesListAdapter
import ru.bespalov.spacex_mvi.presentation.capsules_list.CapsulesListFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .add(R.id.mainContainer, MainFragment())
            .commit()
    }
}