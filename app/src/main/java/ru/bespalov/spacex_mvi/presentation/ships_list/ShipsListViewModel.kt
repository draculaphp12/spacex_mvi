package ru.bespalov.spacex_mvi.presentation.ships_list

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.spacex_mvi.domain.usecase.GetShipsListUseCase
import ru.bespalov.spacex_mvi.presentation.base.BaseViewModel
import ru.bespalov.spacex_mvi.presentation.base.Reducer
import javax.inject.Inject

class ShipsListViewModel @Inject constructor(
    private val getShipsListUseCase: GetShipsListUseCase
) : BaseViewModel<ShipsListState, ShipsListUiEvent>() {
    override val state: Flow<ShipsListState>
        get() = reducer.state

    init {
        viewModelScope.launch {
            getCapsulesList()
        }
    }

    private suspend fun getCapsulesList() {
        try {
            val data = getShipsListUseCase.execute()
            reducer.setState(ShipsListState(false, data, null, ""))
        } catch (e: Exception) {
            reducer.setState(ShipsListState(false, emptyList(), e, ""))
        }
    }

    private fun search(searchId: String = "") {
        viewModelScope.launch {
            try {
                reducer.setState(ShipsListState(true, emptyList(), null, ""))
                val data = getShipsListUseCase.execute(searchId)
                reducer.setState(ShipsListState(false, data, null, ""))
            } catch (e: Exception) {
                reducer.setState(ShipsListState(false, emptyList(), e, ""))
            }
        }
    }

    fun sendEvent(event: ShipsListUiEvent) {
        reducer.sendEvent(event)
    }

    private val reducer = ShipsListReducer(
        ShipsListState(
            true,
            emptyList(),
            null,
            ""
        )
    )

    private inner class ShipsListReducer(
        initial: ShipsListState
    ) : Reducer<ShipsListState, ShipsListUiEvent>(initial) {
        override fun reduce(oldState: ShipsListState, event: ShipsListUiEvent) {
            when (event) {
                is ShipsListUiEvent.ChangeSearch -> {
                    search(event.searchString)
                }
            }
        }
    }
}