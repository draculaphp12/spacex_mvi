package ru.bespalov.spacex_mvi.presentation.capsules_list

import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.presentation.base.UiState

data class CapsulesListState(
    val isLoading: Boolean,
    val data: List<Capsule>,
    val error: Throwable? = null,
    val searchString: String = ""
) : UiState
