package ru.bespalov.spacex_mvi.presentation.ships_list

import ru.bespalov.spacex_mvi.domain.model.Ship
import ru.bespalov.spacex_mvi.presentation.base.UiState

data class ShipsListState(
    val isLoading: Boolean,
    val data: List<Ship>,
    val error: Throwable? = null,
    val searchString: String = ""
) : UiState