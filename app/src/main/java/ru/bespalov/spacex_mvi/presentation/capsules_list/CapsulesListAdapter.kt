package ru.bespalov.spacex_mvi.presentation.capsules_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.bespalov.spacex_mvi.databinding.CapsuleItemBinding
import ru.bespalov.spacex_mvi.domain.model.Capsule

class CapsulesListAdapter(private val items: List<Capsule>) : RecyclerView.Adapter<CapsulesListAdapter.CapsulesListVH>() {
    inner class CapsulesListVH(private val capsuleItem: CapsuleItemBinding) : RecyclerView.ViewHolder(capsuleItem.root) {
        fun bind(capsule: Capsule) {
            capsuleItem.capsuleId.text = capsule.id.toString()
            capsuleItem.capsuleStatus.text = capsule.status
            capsuleItem.capsuleType.text = capsule.type
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CapsulesListVH {
        val rootView = CapsuleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CapsulesListVH(rootView)
    }

    override fun onBindViewHolder(holder: CapsulesListVH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}