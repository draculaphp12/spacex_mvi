package ru.bespalov.spacex_mvi.presentation.ships_list

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch
import ru.bespalov.spacex_mvi.R
import ru.bespalov.spacex_mvi.databinding.FragmentShipsListBinding
import ru.bespalov.spacex_mvi.di.AppComponent
import ru.bespalov.spacex_mvi.domain.model.Ship
import ru.bespalov.spacex_mvi.presentation.base.DebouncingQueryTextListener

class ShipsListFragment : Fragment() {
    private lateinit var binding: FragmentShipsListBinding

    private val viewModel: ShipsListViewModel by viewModels {
        AppComponent.get().shipsListViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppComponent.get().inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        val searchItem: MenuItem = menu.findItem(R.id.search)
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.setOnQueryTextListener(
            DebouncingQueryTextListener { newText ->
                newText?.let {
                    if (it.isEmpty()) {
                        viewModel.sendEvent(ShipsListUiEvent.ChangeSearch(""))
                    } else {
                        viewModel.sendEvent(ShipsListUiEvent.ChangeSearch(newText))
                    }
                }
            }
        )

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        requireActivity().title = getString(R.string.ships_list_title)
        binding = FragmentShipsListBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.state.collect { shipsListState ->
                if (shipsListState.isLoading) {
                    binding.shipProgressBar.visibility = View.VISIBLE
                    binding.shipListView.visibility = View.GONE
                } else {
                    binding.shipProgressBar.visibility = View.GONE
                    if (shipsListState.error != null) {
                        binding.shipErrorTextView.visibility = View.VISIBLE
                    } else {
                        binding.shipListView.visibility = View.VISIBLE
                        binding.shipErrorTextView.visibility = View.GONE
                        setupList(shipsListState.data)
                    }
                }
            }
        }
    }

    private fun setupList(items: List<Ship>) {
        binding.shipListView.adapter = ShipsListAdapter(items)
        binding.shipListView.layoutManager = LinearLayoutManager(requireContext())
    }
}