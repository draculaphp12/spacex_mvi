package ru.bespalov.spacex_mvi.presentation.ships_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.bespalov.spacex_mvi.databinding.ShipItemBinding
import ru.bespalov.spacex_mvi.domain.model.Ship

class ShipsListAdapter(private val items: List<Ship>) : RecyclerView.Adapter<ShipsListAdapter.ShipsListVH>() {
    inner class ShipsListVH(private val shipItem: ShipItemBinding) : RecyclerView.ViewHolder(shipItem.root) {
        fun bind(ship: Ship) {
            Glide.with(itemView.context)
                .load(ship.image)
                .circleCrop()
                .into(shipItem.shipImage)
            shipItem.shipId.text = ship.id.toString()
            shipItem.shipName.text = ship.name.toString()
            if (ship.model != null) {
                shipItem.shipModel.text = ship.model.toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShipsListVH {
        val rootView = ShipItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShipsListVH(rootView)
    }

    override fun onBindViewHolder(holder: ShipsListVH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}