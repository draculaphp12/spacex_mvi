package ru.bespalov.spacex_mvi.presentation.capsules_list

import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.presentation.base.UiEvent

sealed class CapsulesListUiEvent : UiEvent {
    data class ShowData(val items: List<Capsule>) : CapsulesListUiEvent()
    data class ShowError(val e: Throwable) : CapsulesListUiEvent()
    data class ChangeSearch(val searchString: String = "") : CapsulesListUiEvent()
    object ShowProgress : CapsulesListUiEvent()
}