package ru.bespalov.spacex_mvi.presentation.capsules_list

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.spacex_mvi.domain.usecase.GetCapsulesListUseCase
import ru.bespalov.spacex_mvi.presentation.base.BaseViewModel
import ru.bespalov.spacex_mvi.presentation.base.Reducer
import javax.inject.Inject

class CapsuleViewModel @Inject constructor(
    private val getCapsulesListUseCase: GetCapsulesListUseCase
): BaseViewModel<CapsulesListState, CapsulesListUiEvent>() {
    override val state: Flow<CapsulesListState>
        get() = reducer.state

    private val reducer = CapsulesListReducer(
        CapsulesListState(
            true,
            emptyList(),
            null,
            ""
        )
    )

    init {
        viewModelScope.launch {
            getCapsulesList()
        }
    }

    private suspend fun getCapsulesList() {
        try {
            val data = getCapsulesListUseCase.execute()
            sendEvent(CapsulesListUiEvent.ShowData(data))
        } catch (e: Exception) {
            sendEvent(CapsulesListUiEvent.ShowError(e))
        }
    }

    private fun search(searchId: String = "") {
        viewModelScope.launch {
            try {
                sendEvent(CapsulesListUiEvent.ShowProgress)
                val data = getCapsulesListUseCase.execute(searchId)
                sendEvent(CapsulesListUiEvent.ShowData(data))
            } catch (e: Exception) {
                sendEvent(CapsulesListUiEvent.ShowError(e))
            }
        }
    }

    fun sendEvent(event: CapsulesListUiEvent) {
        reducer.sendEvent(event)
    }

    private inner class CapsulesListReducer(
        initial: CapsulesListState
    ) : Reducer<CapsulesListState, CapsulesListUiEvent>(initial) {
        override fun reduce(oldState: CapsulesListState, event: CapsulesListUiEvent) {
            when (event) {
                is CapsulesListUiEvent.ShowProgress -> {
                    setState(oldState.copy(isLoading = true))
                }
                is CapsulesListUiEvent.ShowData -> {
                    setState(oldState.copy(isLoading = false, data = event.items, error = null))
                }
                is CapsulesListUiEvent.ShowError -> {
                    setState(oldState.copy(isLoading = false, error = event.e))
                }
                is CapsulesListUiEvent.ChangeSearch -> {
                    search(event.searchString)
                }
            }
        }
    }
}