package ru.bespalov.spacex_mvi.presentation.capsules_list

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch
import ru.bespalov.spacex_mvi.R
import ru.bespalov.spacex_mvi.databinding.FragmentCapsulesListBinding
import ru.bespalov.spacex_mvi.di.AppComponent
import ru.bespalov.spacex_mvi.domain.model.Capsule
import ru.bespalov.spacex_mvi.presentation.base.DebouncingQueryTextListener

class CapsulesListFragment : Fragment() {
    private lateinit var binding: FragmentCapsulesListBinding

    private val viewModel: CapsuleViewModel by viewModels {
        AppComponent.get().capsuleViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppComponent.get().inject(this)
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        val searchItem: MenuItem = menu.findItem(R.id.search)
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.setOnQueryTextListener(
            DebouncingQueryTextListener { newText ->
                newText?.let {
                    if (it.isEmpty()) {
                        viewModel.sendEvent(CapsulesListUiEvent.ChangeSearch(""))
                    } else {
                        viewModel.sendEvent(CapsulesListUiEvent.ChangeSearch(newText))
                    }
                }
            }
        )

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        requireActivity().title = getString(R.string.capsules_list_title)
        binding = FragmentCapsulesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.state.collect { capsuleState ->
                if (capsuleState.isLoading) {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.capsulesList.visibility = View.GONE
                } else {
                    binding.progressBar.visibility = View.GONE
                    if (capsuleState.error != null) {
                        binding.errorTextView.visibility = View.VISIBLE
                    } else {
                        binding.capsulesList.visibility = View.VISIBLE
                        binding.errorTextView.visibility = View.GONE
                        setupList(capsuleState.data)
                    }
                }
            }
        }
    }

    private fun setupList(items: List<Capsule>) {
        binding.capsulesList.adapter = CapsulesListAdapter(items)
        binding.capsulesList.layoutManager = LinearLayoutManager(requireContext())
        binding.capsulesList.addItemDecoration(
            DividerItemDecoration(binding.capsulesList.context, DividerItemDecoration.VERTICAL)
        )
    }
}